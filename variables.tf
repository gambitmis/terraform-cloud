variable "AWS_ACCESS_KEY_ID" {
  description = "AWS access key"
  type        = string
  default     = ""
}

variable "AWS_SECRET_ACCESS_KEY" {
  description = "AWS secret key"
  type        = string
  default     = ""
}

# Create a VPC
resource "aws_vpc" "terraform_cloud" {
  cidr_block = "10.0.0.0/16"
}